require.config({
    shim: {
        'facebook': {
            exports: 'FB'
        }
    },

    deps: ['app'],



    paths: {
        'facebook': 'http://connect.facebook.net/en_US/all',
        'jquery': 'vendor/lib/jquery/jquery-1.11.0.min',
        'knockout': 'vendor/lib/knockout/knockout-3.0.0',
        'google': 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAqTdgk9SW_lz6y4VA6Mw2laF2BTBQQvYQ&sensor=false',

        'facebookLoader': 'app/globals/facebookloader',
        'gmaps': 'app/globals/gmaps',
        'facebooklogin': 'app/utilities/facebooklogin',

        'photosOfCyprus': 'app/viewModels/photosOfCyprus',

        'viewModels': 'app/viewModels/',
        'utilities': 'app/utilities/',
        'models': 'app/models/'
    }
});
