define(function(require) {

    var $ = require('jquery');

        require('gmaps');
        require('facebookLoader');
        require('facebooklogin');

    var MapViewModel =  require('viewModels/mapViewModel'),
        FBGroupViewModel = require('viewModels/fbGroupViewModel'),
        mapVM;

    var PhotosOfCyprus = function () {

        this.init = function () {
            mapVM = new MapViewModel({address:'Cyprus', mapCanvasName:'map_canvas'});

            $(document).bind('mapReady', function () {
                fbAlbumVM = new FBGroupViewModel({groupUrl: '/Photos.of.Cyprus/albums'});
            });
         };
    };

    return new PhotosOfCyprus();
});