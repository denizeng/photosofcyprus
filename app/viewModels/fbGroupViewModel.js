define(function (require) {
    require('facebook');

    var loginManager = require('facebooklogin'),
        geoCoder = require('utilities/geocoder'),
        FacebookAPI = require('utilities/facebookapi'),
        Album = require('models/album'),
        googleMaps = require('gmaps'),
        locationCache = JSON.parse(require('vendor/lib/require-plugins/text!app/data/locationMappings.txt'));

    return function (options) {

        var groupUrl = options.groupUrl,
            albums = [],
            facebookAPI = new FacebookAPI(groupUrl);

        var _getMappedAlbums = function (albumData) {
            return $.map(albumData.data, function (item) {
                return new Album(item);
            });
        };

        this.init = function () {
            this.subscribeToLoginStatus();
        };

        this.subscribeToLoginStatus = function () {
            console.log('subscribing to login status');
            var isLoggedIn = loginManager.subscribeToLoginStatus($.proxy(this.handleFBLoggedIn, this));

            if (isLoggedIn) {
                this.handleFBLoggedIn();
            }
        };

        this.handleFBLoggedIn = function () {
            this.getAlbumsFromFB();
        };

        this.getAlbumsFromFB = function () {
            console.log('is logged in getting albums');
            facebookAPI.getAlbums($.proxy(this.handleAlbumResponse, this));
        };


        //TODO: geocoding logic into a different class
        this.handleAlbumResponse = function (response) {
            var mappedAlbums = _getMappedAlbums(response),
                mappedAlbumsLength;

            if (mappedAlbums) {
                mappedAlbumsLength = mappedAlbums.length;
                albums = albums.concat(mappedAlbums);

                for (var albumIndex = 0; albumIndex < mappedAlbumsLength; albumIndex++) {
                    var currentAlbum = mappedAlbums[albumIndex],
                        locationLength = currentAlbum.locations.length;

                    for (var locationIndex = 0; locationIndex < locationLength; locationIndex++) {
                        var currentLocation = currentAlbum.locations[locationIndex];

                        //if the stored location doesn't have either greek or turkish go to geocoding
                        var locationFromStorage = this.locationFromStorage(currentLocation);

                        if (locationFromStorage !== null) {
                            $(document).trigger('addPin',{geocoderResult: locationFromStorage, album:currentAlbum, location:currentLocation });
                        }
                        else if (currentLocation.greekName.toLowerCase().indexOf('zeki') === -1) {
                            console.log(currentLocation, "-> not found in storage");

                            //TODO: refactor to a method in seperate class or here, refactor location name sanitisation
                            geoCoder.geocode(currentLocation.greekName, $.proxy(this.handleAlbumGeocoding, this, currentAlbum,currentLocation, locationIndex));
                        }
                    }
                }
            }
        };

        //TODO: storage to different class
        this.locationFromStorage = function (location) {
            var storedLocation = null;
            for (var i =0; i <locationCache.length;i++) {
                if (locationCache[i].greekName === location.greekName || locationCache[i].turkishName === location.turkishName) {
                    locationCache[i].geometry.location = new google.maps.LatLng(locationCache[i].geometry.k,locationCache[i].geometry.A);
                    storedLocation =  [locationCache[i]];
                }
            }
            return storedLocation;
        };

        //TODO:different class
        this.handleAlbumGeocoding = function (currentAlbum, currentLocation, locationIndex, geocoderResult) {
                if (geocoderResult !== null) {
                    //save geometry data to location
                    currentAlbum.locations[locationIndex].geometry = geocoderResult[0].geometry.location;

                    console.log('adding to cache:', currentAlbum.locations[locationIndex].name);
                    locationCache.push(currentAlbum.locations[locationIndex]);

                    $(document).trigger('addPin',{geocoderResult: geocoderResult, album:currentAlbum, location:currentLocation });
                } else if (currentAlbum.locations[locationIndex].turkishName !== currentLocation){
                    //try turkish name if it is not current location
                   var turkishName = currentAlbum.locations[locationIndex].turkishName;
                    //console.log("no results for ", currentLocation, " - trying: ", turkishName);
                    geoCoder.geocode(turkishName, $.proxy(this.handleAlbumGeocoding, this, currentAlbum, turkishName, locationIndex));
                } else if (currentAlbum.locations[locationIndex].turkishName === currentLocation) {
                    console.log("no turkish or greek results for:", currentAlbum.locations[locationIndex].name);
                }
        };

        this.init();
    };
});