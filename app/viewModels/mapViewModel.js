define(function (require) {
    var currentMap,
        geocoder = require('utilities/geocoder'),
        googleMaps = require('gmaps'),
        $ = require('jquery');

    return function (mapOptions) {

        var pins,
            isMapReady = false,
            self = this;

        this.init = function () {
            geocoder.geocode(mapOptions.address, $.proxy(this.handleGeocoderStatusSuccess,this))
        };

        this.isReady = function () {
            return isMapReady;
        };

        this.handleGeocoderStatusSuccess = function (results) {
            var newMapOptions = this.getNewMapOptions(results);

            currentMap = new googleMaps.Map(document.getElementById(mapOptions.mapCanvasName), newMapOptions);

            currentMap.setCenter(results[0].geometry.location);

            this.handleMapReady();
        };

        this.getNewMapOptions = function (geoCoderResults) {
            return {
                zoom: 9,
                center: geoCoderResults[0].geometry.location,
                mapTypeId: googleMaps.MapTypeId.ROADMAP
            };
        };

        this.handleMapReady = function () {
            isMapReady = true;
            $(document).trigger("mapReady", currentMap);
            self.bindToAddPinEvent();
            this.addLoginControl();
        };

        this.bindToAddPinEvent = function () {
            $(document).bind("addPin", this.handleAddPin);
        };

        this.handleAddPin = function (event,data) {
            var marker = self.addNewMarker(data);
            self.addClickEventToMarker(marker, data.album.link);
        };

        this.addClickEventToMarker = function (marker,link) {
            googleMaps.event.addListener(marker, 'click', function () {
                window.open(link);
            });
        };

        this.addNewMarker = function (data) {
            var marker = new google.maps.Marker({
                map: currentMap,
                position: data.geocoderResult[0].geometry.location,
                title: data.location.name
            });

            return marker;
        };

        //TODO: inject controls to be rather than this
        this.addLoginControl = function () {

            var homeControlDiv = document.createElement('div');
            var homeControl = new LoginControl(homeControlDiv, currentMap);

            homeControlDiv.index = 1;
            currentMap.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);
        };

        this.init();
    };


    //Refactor to a different file
    function LoginControl(controlDiv) {

        var control = this;

        controlDiv.style.padding = '5px';

        // Set CSS for the control border
        var goHomeUI = document.createElement('div');
        goHomeUI.style.backgroundColor = 'white';
        goHomeUI.style.borderStyle = 'solid';
        goHomeUI.style.borderWidth = '2px';
        goHomeUI.style.cursor = 'pointer';
        goHomeUI.style.textAlign = 'center';
        goHomeUI.title = 'Login';
        controlDiv.appendChild(goHomeUI);

        // Set CSS for the control interior
        var goHomeText = document.createElement('div');
        goHomeText.style.fontFamily = 'Arial,sans-serif';
        goHomeText.style.fontSize = '12px';
        goHomeText.style.paddingLeft = '4px';
        goHomeText.style.paddingRight = '4px';
        goHomeText.innerHTML = '<b>Login</b>';
        goHomeUI.appendChild(goHomeText);


        google.maps.event.addDomListener(goHomeUI, 'click', function() {

            var loginManager = require('facebooklogin');

            loginManager.attemptFBLogin();

        });
    }

});