define(function (require) {

    var Location = function (options) {

        this.name = options.name;

        this.turkishName = options.turkishName;

        this.greekName = options.greekName;
    };

    return Location;
});