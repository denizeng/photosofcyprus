define(function (require) {

    var locationParser = require('utilities/locationParser'),
        linkCreator = require('utilities/linkCreator');

    var Album = function (data) {

        this.id = data.id;
        this.link = linkCreator.createLink(data.link);
        this.name = data.name;
        this.description = data.description;
        this.coverPhotoID = data.cover_photo;
        this.coverImagePath = data.coverImagePath;

        this.locations = locationParser.getLocationsFromDescription(data.description);
    };

    return Album;
});

