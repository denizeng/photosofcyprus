define(function (require) {
    var config = require('app.config');
    require('facebook');

    FB.init({
        appId:  '133166390195477',
        channelUrl: 'http://www.denizengin.com/experiments/photosofcyprus/channel.html', // Channel File for x-domain communication
        status: true, // check the login status upon init?
        cookie: true, // set sessions cookies to allow your server to access the session?
        xfbml: true  // parse XFBML tags on this page?
    });

    return window.FB;
});
