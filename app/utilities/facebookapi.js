define(function (require) {
    var FB = require('facebookLoader'),
        FBAPI;

    FBAPI = function (apiUrl) {
        var self = this;

        var _getNextPage = function (response, callback) {
            if (response && typeof response.paging !== 'undefined' && typeof response.paging.next !== 'undefined') {
                var nextUrl = response.paging.next;
                self.getAlbums(callback, nextUrl);
            }
        };

        this.getAlbums = function (callback, url) {
            FB.api(url ? url : apiUrl, function (response) {
                callback(response);
                _getNextPage(response, callback);
            });
        };
    };

    return FBAPI;
});