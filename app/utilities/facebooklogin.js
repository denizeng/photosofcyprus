define(function (require) {
    var FB = require('facebookLoader'),
        $ = require('jquery'),
        ko = require('knockout');

    var loginManager = function () {

        var _isLoggedIn = false;

        this.init = function () {
            FB.Event.subscribe("auth.authResponseChange", $.proxy(this.loginResultHandler,this))
            console.log("init called, checking FB login state");
            FB.getLoginStatus($.proxy(this.loginStatusHandler, this), true);
        };

        this.isLoggedIn = function () {
            return _isLoggedIn;
        };

        this.triggerIsLoggedIn = function () {
            $(document).trigger("fbLoggedIn");
            _isLoggedIn = true;
        };

        this.handleFBConnected = function () {
            this.triggerIsLoggedIn();
        };

        this.handleFBUnauthorized = function () {

        };

        this.attemptFBLogin = function () {
            FB.login($.proxy(this.loginResultHandler,this));
        };

        this.loginResultHandler = function (loginResponse) {
             if (loginResponse.authResponse) {
                 this.triggerIsLoggedIn();
             } else {
                console.log('User cancelled login or did not fully authorize.');
                 $(document).trigger("fbLoggedOut");
             }
        };

        this.loginStatusHandler = function (response) {
            if (response.status === 'connected') {
                console.log('result received');
                this.handleFBConnected();
            } else if (response.status === 'not_authorized') {
                this.handleFBUnauthorized();
            } else {
                this.attemptFBLogin();
            }
        };

        this.subscribeToLoginStatus = function (callback) {
            $(document).bind("fbLoggedIn",callback);
            return this.isLoggedIn();
        };

        this.init();
    };

    return new loginManager();
});
