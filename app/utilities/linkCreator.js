define(function (require) {

    var LinkCreator = function () {

        this.createLink = function (originalLink) {

            //https://www.facebook.com/album.php?fbid=10150844838923160&id=252543048159&aid=411276
            var a = "a." + originalLink.split('?')[1].split('&')[0].split('=')[1],
                b = originalLink.split('?')[1].split('&')[1].split('=')[1],
                c = originalLink.split('?')[1].split('&')[2].split('=')[1];

            return "https://www.facebook.com/media/set/?set=" + a + "." + b + "." + c + "&type=1";
        };
    };

    return new LinkCreator();
});