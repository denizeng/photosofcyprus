define(function (require) {

    var Location = require('models/location');

    var LocationParser = function () {

        var getGreekName = function (description) {
            var name = description.replace('(', '[').replace(')', ']');
            words = name.match(/[^[\]]+(?=])/g);
            if (words && words.length > 0) {
                return words[0];
            } else {
                return (name ? name : 'no name');
            }
        };

        var getTurkishName = function (description) {
            var name =description.replace(/ *\([^)]*\) */g, "");return name;
        };

        var getCleanDescription = function (description) {
            var cleanedDescription = "";
            if (description && typeof description === 'string') {
                cleanedDescription = description.replace('Photos by', '').replace('ZEK? GÜRSEL', '').replace('...', '').replace('..', '').replace('.', '').replace(':', '').trim();
            }
            return cleanedDescription;
          };

        var mapLocationsToLanguages = function (splittedDescription) {
            var mappedLocations = $.map(splittedDescription, function (locationName) {
                var locationItem = new Location({name: locationName, greekName:  getGreekName(locationName), turkishName: getTurkishName(locationName)});

                return locationItem;
            });

            return mappedLocations;
        };

        this.getLocationsFromDescription = function (description) {
            var locations = [],
                cleanedDescription = getCleanDescription(description),
                splittedDescription;

            if (cleanedDescription) {
                splittedDescription = cleanedDescription.split(',');

                if (splittedDescription.length > 0) {
                    locations = mapLocationsToLanguages(splittedDescription)
                }
            }

            return locations;
        };

    };

    return new LocationParser();
});