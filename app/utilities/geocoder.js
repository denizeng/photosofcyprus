define(function (require) {
    var googleMaps = require('gmaps'),
        logger = require('utilities/logger');


    var GeoCoder = function () {

        var self = this,
            currentGeocoder;

        this.init = function () {
            currentGeocoder = new googleMaps.Geocoder();
        };

        this.geocode = function (location, callback) {
            if (location && typeof location === "string") {
                currentGeocoder.geocode({ 'address': location + ',Cyprus' }, function (results, status) {
                    if (status == googleMaps.GeocoderStatus.OK) {
                        callback(results);
                    } else {
                        self.handleGeocoderStatusFailure(status, location, callback);
                    }
                });
            }
        };

        this.handleGeocoderStatusFailure = function (status, location, callback) {
            logger.log("Error getting details from geocoder -- status:" + status + " - location:" + location);

            if (status === googleMaps.GeocoderStatus.OVER_QUERY_LIMIT) {
                setTimeout(function () { self.geocode(location,callback);}, 200);
            }
            else if (status === googleMaps.GeocoderStatus.ZERO_RESULTS) {
                callback(null);
            }
        };

        this.init();

    };

    return new GeoCoder();

});