define(function (require) {

    var Logger = function () {

        var currentLogger;

        this.init = function () {
            currentLogger = typeof(console) === "object" && typeof(console.log) === "function" ? console : undefined;
        };

        this.log = function () {
            if (currentLogger) {
                currentLogger.log(arguments.split(","));
            }
        };
    };

    return new Logger();
});